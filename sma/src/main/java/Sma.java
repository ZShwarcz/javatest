import java.util.ArrayList;


public class Sma {

    private ArrayList<Double> data =  new ArrayList<Double>(); // начальные данные
    public ArrayList<Double> ma = new ArrayList<Double>();    //  скользящее среднее
    private int window;  // размер окна

    Sma(ArrayList<String> val, int window) {
        this.window = window;
        for (int i = 0; i < val.size()-1; i++) {
            double temp = Double.parseDouble(val.get(i));
            data.add(temp);

        }

    }

    // расчёт скользящей средней и запись в ma
    private void SmaCalc() {
        //просчёт скользящей средней
        for (int i = 0; i < data.size(); i++) {
            double summ = 0;
            if (i + window <= data.size()) {
                for (int j = 0; j < window; j++) {
                    int itter = i + j;
                    summ += data.get(itter);
                }
                ma.add(summ / window);
            }
        }
    }

    public ArrayList<Double> GetValueMa(){
        SmaCalc();
        return ma;
    }
    public ArrayList<Double> GetValueData(){
        return data;
    }
    }

