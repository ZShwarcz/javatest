import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class Main{

    public static String repeat(int count, String with) {
        return new String(new char[count]).replace("\0", with);
    }

    public static void main(String[] args){
        ArrayList<String> arr = new ArrayList<String>();  //список почт
        Scanner in = new Scanner(System.in);
        // считывание данных из файла
        try {
            File file = new File("/home/user/javat/javatest/mbox/src/main/java/mbox.txt");
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            while (line != null) {
                arr.add(line);
                line = reader.readLine();

            }
            MBox start = new MBox(arr);


            // красивый вывод подсчитанных почт

            Map<String, Integer> out = start.GetCalcMails();
            for (Map.Entry<String, Integer> pair : out.entrySet())
            {
                int len;
                String key = pair.getKey();
                Integer value = pair.getValue();
                    if(key.length() < 30){
                        len = 30 - key.length();
                        for(int j = 0; j <= len; j++){
                            key = key + " ";
                        }
                    }

                System.out.println(key + "" + repeat(value, "*") + value);
            }
            System.out.println("\n**************************************************\n");

            System.out.println("Среднее значение X-DSPAM-Confidence = " + start.AValue());

            System.out.println("\n**************************************************\n");


            start.getSpamer();





        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}