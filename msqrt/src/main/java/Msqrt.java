public class Msqrt {
    Msqrt(){}

    static public double GetSqrt (double number){
        if(number == 0){
            System.out.println("Infinity");
        }
        else if(number < 0){
            System.out.println("Nan");
        }
        else{
            double temp = 0.0;
            double tnumb = 5;

            for (int i = 0; i < 100000000; i++) {
                tnumb = 0.5f * (tnumb + number / tnumb);
                if (tnumb == temp)
                    break;
                temp = tnumb;
            }
            return temp;
        }
        return 0;
    }
}